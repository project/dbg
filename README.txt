How to use:

1. printing the dump on the website:
   dbg($var)->show();

2. saving to file the dump:
   dbg($var)->writeToFile();